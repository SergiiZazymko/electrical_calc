import MySQLdb
db1 = MySQLdb.connect(host="localhost",  # your host, usually localhost
                      user="portal",  # your username
                      passwd="Pen7D5sBzMBYXaAc",  # your password
                      db= "portal")        # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db1.cursor()

# Use all the SQL you like
cur.execute("SELECT * FROM YOUR_TABLE_NAME")

# print all the first cell of all the rows
for row in cur.fetchall():
    print(row[0])
    break

db1.close()