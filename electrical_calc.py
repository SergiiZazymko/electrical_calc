"""
This is the electrical calculation program.
Version 0.1 beta
"""

from tkinter import *
import math


def calculate(event):
    """
    This function is for calculation power or current.
    If user enter current - function calculates power.
    And if user enter power - function calculates current.
    """
    u = circuit.get()
    cos = cos_phi.get()
    try:
        user_enter = float(enter_enter.get().replace(',', '.'))
    except ValueError as s:
        result_label.configure(text='Please input correct value')
    else:
        if len(str(user_enter)) > 12:
            result_label.configure(text='Your value is too long')
        else:
            if powcur.get() == 1:
                i = user_enter
                if u == 220:
                    p = u * i * cos / 1000
                elif u == 380:
                    p = math.sqrt(3) * u * i * cos / 1000
                result_label.configure(text='I = %.2f A\nP = %.2f kW' % (i, p))
            else:
                p = user_enter
                if u == 220:
                    i = p / (u * cos) * 1000
                elif u == 380:
                    i = p / (math.sqrt(3) * u * cos) * 1000
                result_label.configure(text='P = %.2f kW\nI = %.2f A' % (p, i))
                # print('I = %.2f A\nP = %.2f kW' % (I, P))


def current_label(event):
    """
    This function change the label of user entry.
    power -> current
    """
    enter_label.configure(text='Enter the value of current, A')


def power_label(event):
    """
    This function change the label of user entry.
    current -> power
    """
    enter_label.configure(text='Enter the value of power, kW')


bg_color = 'steelblue'
fg_color = 'white'
main_font = 'Verdana 12'
common_font = 'Verdana 11'

root = Tk()
root.minsize(350, 500)
root.maxsize(350, 500)
root.title('Electrical calculation - 0.1 Beta')
root['bg'] = bg_color

# Power or current

powcur_frame = Frame(root, bg=bg_color, bd=5)
powcur_frame.grid(column=0, row=0, pady=10)

powcur_lable = Label(powcur_frame, bg=bg_color, fg=fg_color, font=main_font, text='What do you want to calculate?')
powcur_lable.grid(column=0, row=0, padx=0)

powcur = IntVar()
powcur.set(0)
powcur_current = Radiobutton(powcur_frame, variable=powcur, value=0, text='I know power, want to calculate current',
                             bg=bg_color, fg='black', font=common_font)
powcur_current.grid(column=0, row=1, padx=0)
powcur_current.bind('<Button-1>', power_label)

powcur_power = Radiobutton(powcur_frame, variable=powcur, value=1, text='I know current, want to calculate power',
                           bg=bg_color, fg='black', font=common_font)
powcur_power.grid(column=0, row=2)
powcur_power.bind('<Button-1>', current_label)

# Type of circuit

circ_frame = Frame(root, bg=bg_color, bd=5)
circ_frame.grid(column=0, row=1, pady=10)

circ_label = Label(circ_frame, bg=bg_color, fg=fg_color, font=main_font, text='Choose type of circuit')
circ_label.grid(column=0, row=0)

circ_frame1 = Frame(circ_frame, bg=bg_color)
circ_frame1.grid(column=0, row=1)

circuit = IntVar()
circuit.set(220)
circ_220 = Radiobutton(circ_frame1, variable=circuit, value=220, bg=bg_color, fg='black', font=common_font,
                       text='220V 1 phase')
circ_220.grid(column=0, row=1)

cirk_380 = Radiobutton(circ_frame1, variable=circuit, value=380, bg=bg_color, fg='black', font=common_font,
                       text='380V 3 phases')
cirk_380.grid(column=1, row=1)
# cos phi

cos_frame = Frame(root, bg=bg_color)
cos_frame.grid(column=0, row=4, pady=5)

cos_lable = Label(cos_frame, bg=bg_color, fg=fg_color, font=main_font, text='Choose the value of cos(phi)')
cos_lable.grid(column=0, row=0, pady=5)

cos_phi = DoubleVar()
cos_phi.set(0.9)
cos_scale = Scale(cos_frame, length=300, orient=HORIZONTAL, from_=0.8, to=1, tickinterval=0.05, resolution=0.01,
                  bg=bg_color, variable=cos_phi)
cos_scale.grid(column=0, row=1)

# The value

enter_frame = Frame(root, bg=bg_color, bd=5)
enter_frame.grid(column=0, row=5, pady=10)

enter_label = Label(enter_frame, bg=bg_color, fg=fg_color, font=main_font, text='Enter the value of power, kW')
enter_label.grid(column=0, row=0, pady=5)

enter_frame1 = Frame(enter_frame, bg=bg_color)
enter_frame1.grid(column=0, row=1)

enter_enter = Entry(enter_frame1, width=10, font=common_font, bd=3)
enter_enter.grid(column=0, row=0)

enter_button = Button(enter_frame1, width=10, bg=bg_color, fg='red', font=common_font, text='Calculate')
enter_button.grid(column=1, row=0, padx=15)

enter_button.bind('<Button-1>', calculate)
enter_enter.bind('<Return>', calculate)

# Result

result_frame = Frame(root, bg=bg_color)
result_frame.grid(column=0, row=6, pady=20)

result_label = Label(result_frame, bg=bg_color, fg='darkblue', font='Verdana 14')
result_label.grid(column=0, row=0)

root.mainloop()
